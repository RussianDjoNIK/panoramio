﻿namespace Panoramio.Common
{
    public struct RectangleCoordinateAggregator
    {
        private readonly Coordinate _leftTopCoord;
        private readonly Coordinate _rightBottomCoord;

        public Coordinate LeftTopCoord
        {
            get { return _leftTopCoord; }
        }

        public Coordinate RightBottomCoord
        {
            get { return _rightBottomCoord; }
        }

        public RectangleCoordinateAggregator(Coordinate leftTopCoord, Coordinate rightBottomCoord)
        {
            _leftTopCoord = leftTopCoord;
            _rightBottomCoord = rightBottomCoord;
        }
    }
}
