﻿using Windows.Foundation;

namespace Panoramio.Common
{
    public struct Coordinate
    {
        private readonly Point _position;
        private readonly Point _location;

        public Point Position
        {
            get { return _position; }
        }

        public Point Location
        {
            get { return _location; }
        }

        public Coordinate(Point pos, Point loc)
        {
            _position = pos;
            _location = loc;
        }
    }
}
