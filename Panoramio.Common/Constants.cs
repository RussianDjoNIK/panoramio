﻿namespace Panoramio.Common
{
    public class Constants
    {
        public static class ApiValues
        {
            public const string Original = "original";
            public const string Medium = "medium";
            public const string Small = "small";
            public const string Thumbnail = "thumbnail";
            public const string Square = "square";
            public const string MiniSquare = "mini_square";
            public const string Public = "public";
            public const string Full = "full";
            public const string UserId = "";
        }

        public static class DtoFields
        {
            public const string Count = "count";
            public const string HasMore = "has_more";
            public const string MapLocation = "map_location";
            public const string Lat = "lat";
            public const string Lon = "lon";
            public const string PanoramioZoom = "panoramio_zoom";
            public const string Height = "height";
            public const string Width = "width";
            public const string Latitude = "latitude";
            public const string Longitude = "longitude";
            public const string OwnerId = "owner_id";
            public const string OwnerName = "owner_name";
            public const string OwnerUrl = "owner_url";
            public const string PhotoFileUrl = "photo_file_url";
            public const string PhotoId = "photo_id";
            public const string PhotoTitle = "photo_title";
            public const string PhotoUrl = "photo_url";
            public const string UploadDate = "upload_date";
            public const string Photo = "photos";
        }
    }
}
