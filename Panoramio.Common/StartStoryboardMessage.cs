﻿namespace Panoramio.Common
{
    public class StartStoryboardMessage
    {
        public string StoryboardName { get; set; }
    }
}
