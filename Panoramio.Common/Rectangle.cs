﻿namespace Panoramio.Helpers
{
    public struct Rectangle<T>
    {
        #region Fields

        private readonly T _left;
        private readonly T _top;
        private readonly T _width;
        private readonly T _height;

        #endregion

        #region Properties

        public T Left
        {
            get { return _left; }
        }

        public T Top
        {
            get { return _top; }
        }

        public T Width
        {
            get { return _width; }
        }

        public T Height
        {
            get { return _height; }
        }

        #endregion

        #region Contruct

        public Rectangle(T left, T top, T width, T height)
        {
            _left = left;
            _top = top;
            _width = width;
            _height = height;
        }

        #endregion
    }
}