﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace Panoramio.Common
{
    public class ObservableRangeCollection<T> : ObservableCollection<T>
    {

        public bool IsNotifying { get; private set; }

        public void AddRange(IEnumerable<T> newItems)
        {
            var items = newItems.ToList();
            IsNotifying = false;

            var index = Count;
            foreach (var item in items)
            {
                InsertItem(index, item);
                index++;
            }

            IsNotifying = true;

            OnPropertyChanged(new PropertyChangedEventArgs("Count"));
            OnPropertyChanged(new PropertyChangedEventArgs("Item[]"));
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (IsNotifying)
            {
                base.OnCollectionChanged(e);
            }
        }
    }
}
