﻿using System;
using System.Diagnostics;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Data;
using Panoramio.Common;

namespace Panoramio.Converters
{
    public class MapTappedEventToCoordinateConverter : IValueConverter
    {
        //TODO AreaHeight to settings
        public const double AreaHeight = 20.0;

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var args = value as MapInputEventArgs;
            Debug.Assert(null != args);

            var mapControl = parameter as MapControl;
            Debug.Assert(null != mapControl);

            var pos = args.Position;

            var leftTopPos = new Point(pos.X - AreaHeight, pos.Y - AreaHeight);
            var rightBottomPos = new Point(pos.X + AreaHeight, pos.Y + AreaHeight);

            Geopoint leftTopLoc;
            Geopoint rightBottomLoc;

            mapControl.GetLocationFromOffset(leftTopPos, out leftTopLoc);
            mapControl.GetLocationFromOffset(rightBottomPos, out rightBottomLoc);

            var result = new RectangleCoordinateAggregator
            (
             new Coordinate(new Point(leftTopPos.X, leftTopPos.Y), new Point(leftTopLoc.Position.Longitude, leftTopLoc.Position.Latitude)),
             new Coordinate(new Point(rightBottomPos.X, rightBottomPos.Y), new Point(rightBottomLoc.Position.Longitude, rightBottomLoc.Position.Latitude))
            );

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
