﻿using System;
using System.Diagnostics;
using Windows.Devices.Geolocation;
using Windows.UI.Xaml.Data;
using Panoramio.ViewModel.EntitiesVM;

namespace Panoramio.Converters
{
    public class PhotoToGeopointConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var photo = value as PhotoViewModel;
            Debug.Assert(photo != null);

            var geoPosition = new BasicGeoposition { Longitude = photo.Longitude, Latitude = photo.Latitude };
            return new Geopoint(geoPosition);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
