﻿using System;
using System.Diagnostics;
using Windows.UI.Xaml.Data;
using Bing.Maps;
using Panoramio.ViewModel.EntitiesVM;

namespace Panoramio.Converters
{
    public class PhotoToLocationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var photo = value as PhotoViewModel;
            Debug.Assert(photo != null);

            return new Location(photo.Latitude, photo.Longitude);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
