﻿using System;
using System.Diagnostics;
using Windows.Foundation;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Bing.Maps;
using Panoramio.Common;

namespace Panoramio.Converters
{
    public class MapTappedEventToCoordinateConverter : IValueConverter
    {
        //TODO AreaHeight to settings
        private const double AreaHeight = 40.0;

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var tappedEventArgs = value as TappedRoutedEventArgs;
            Debug.Assert(null != tappedEventArgs);

            var mapControl = parameter as Map;
            Debug.Assert(null != mapControl);

            var pos = tappedEventArgs.GetPosition(mapControl);

            var leftTopPos = new Point(pos.X - AreaHeight, pos.Y - AreaHeight);
            var rightBottomPos = new Point(pos.X + AreaHeight, pos.Y + AreaHeight);

            Location leftTopLoc;
            Location rightBottomLoc;

            var res = mapControl.TryPixelToLocation(leftTopPos, out leftTopLoc);
            Debug.Assert(res);

            res = mapControl.TryPixelToLocation(rightBottomPos, out rightBottomLoc);
            Debug.Assert(res);

            var result = new RectangleCoordinateAggregator
            (
             new Coordinate(new Point(leftTopPos.X, leftTopPos.Y), new Point(leftTopLoc.Longitude, leftTopLoc.Latitude)),
             new Coordinate(new Point(rightBottomPos.X, rightBottomPos.Y), new Point(rightBottomLoc.Longitude, rightBottomLoc.Latitude))
            );

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
