﻿using Windows.UI.Xaml.Media.Animation;
using GalaSoft.MvvmLight.Messaging;
using Panoramio.Common;

namespace Panoramio
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            InitializeComponent();
            
            Messenger.Default.Register<StartStoryboardMessage>(this, x => StartStoryboard(x.StoryboardName));
        }

        private void StartStoryboard(string storyboardName)
        {
            var storyboard = FindName(storyboardName) as Storyboard;
            if (storyboard != null)
            {
                storyboard.RepeatBehavior = new RepeatBehavior(1);
                storyboard.Begin();
            }
        }
    }
}