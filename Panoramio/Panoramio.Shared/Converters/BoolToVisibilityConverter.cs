﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Panoramio.Converters
{
    public class BoolToVisibilityConverter : IValueConverter
    {
        public Visibility TrueVisibility { get; set; }
        public Visibility FalseVisibility { get; set; }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return (bool) value ? TrueVisibility : FalseVisibility;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return (Visibility) value == TrueVisibility;
        }
    }
}
