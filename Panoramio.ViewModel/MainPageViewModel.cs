﻿using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Panoramio.Common;
using Panoramio.Helpers;
using Panoramio.Model.API;
using Panoramio.ViewModel.EntitiesVM;

namespace Panoramio.ViewModel
{
    public class MainPageViewModel : ViewModelBase
    {
        #region Fields

        private readonly ObservableRangeCollection<PhotoViewModel> _photoUriItems;
        
        private bool _isLoading;
        private Rectangle<double> _rectangle;

        private RelayCommand<RectangleCoordinateAggregator> _newAreaCommand;

        #endregion

        #region Commands

        public ICommand NewAreaCommand
        {
            get { return _newAreaCommand ?? (_newAreaCommand = new RelayCommand<RectangleCoordinateAggregator>(ExecuteNewArea)); }
        }

        #endregion

        #region Properties

        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                _isLoading = value;
                
                RaisePropertyChanged();
            }
        }

        public Rectangle<double> Rectangle
        {
            get { return _rectangle; }
            set
            {
                _rectangle = value;

                RaisePropertyChanged();
            }
        }

        public ObservableRangeCollection<PhotoViewModel> PhotoUriItems
        {
            get { return _photoUriItems; }
        }

        #endregion

        #region Contruct

        public MainPageViewModel()
        {
            _photoUriItems = new ObservableRangeCollection<PhotoViewModel>();
        }

        #endregion

        #region Public Methods


        #endregion

        #region Private Methods

        private async void ExecuteNewArea(RectangleCoordinateAggregator coordAgregator)
        {
            var leftTopX = coordAgregator.LeftTopCoord.Position.X;
            var leftTopY = coordAgregator.LeftTopCoord.Position.Y;
            var rightBottomX = coordAgregator.RightBottomCoord.Position.X;
            var rightBottomY = coordAgregator.RightBottomCoord.Position.Y;

            Rectangle = new Rectangle<double>(leftTopX, leftTopY, rightBottomX - leftTopX, rightBottomY - leftTopY);

            Messenger.Default.Send(new StartStoryboardMessage { StoryboardName = "ShowHideRectangle" });

            IsLoading = true;

            try
            {
                var res =
                    await
                        PanoramioApi.GetPanoramioResponce(coordAgregator.LeftTopCoord.Location.X,
                            coordAgregator.LeftTopCoord.Location.Y,
                            coordAgregator.RightBottomCoord.Location.X,
                            coordAgregator.RightBottomCoord.Location.Y);
                if (null == res || !string.IsNullOrEmpty(res.ErrorMessage))
                {
                    //TODO error message throw IMessageService
                    return;
                }
                
                PhotoUriItems.Clear();
                PhotoUriItems.AddRange(res.Photos.Select(p => new PhotoViewModel(p)));
            }
            finally
            {
                IsLoading = false;
            }
        }

        #endregion
    }
}
