﻿using System;
using System.Diagnostics;
using GalaSoft.MvvmLight;
using Panoramio.Model;

namespace Panoramio.ViewModel.EntitiesVM
{
    public class PhotoViewModel : ViewModelBase
    {
        #region Fields

        private readonly Photo _photo;

        #endregion

        #region Properties

        public double Width
        {
            get { return _photo.Width; }
            set
            {
                _photo.Width = value;

                RaisePropertyChanged();
            }
        }

        public double Height
        {
            get { return _photo.Height; }
            set
            {
                _photo.Height = value;

                RaisePropertyChanged();
            }
        }

        public double Latitude
        {
            get { return _photo.Latitude; }
            set
            {
                _photo.Latitude = value;

                RaisePropertyChanged();
            }
        }

        public double Longitude
        {
            get { return _photo.Longitude; }
            set
            {
                _photo.Longitude = value;

                RaisePropertyChanged();
            }
        }

        public int OwnerId
        {
            get { return _photo.OwnerId; }
            set
            {
                _photo.OwnerId = value;

                RaisePropertyChanged();
            }
        }

        public string PhotoFileUrl
        {
            get { return _photo.PhotoFileUrl; }
            set
            {
                _photo.PhotoFileUrl = value;

                RaisePropertyChanged();
            }
        }

        public string PhotoTitle
        {
            get { return _photo.PhotoTitle; }
            set
            {
                _photo.PhotoTitle = value;

                RaisePropertyChanged();
            }
        }


        public int PhotoId
        {
            get { return _photo.PhotoId; }
            set
            {
                _photo.PhotoId = value;

                RaisePropertyChanged();
            }
        }

        public string OwnerName
        {
            get { return _photo.OwnerName; }
            set
            {
                _photo.OwnerName = value;

                RaisePropertyChanged();
            }
        }

        public string PhotoUrl
        {
            get { return _photo.PhotoUrl; }
            set
            {
                _photo.PhotoUrl = value;

                RaisePropertyChanged();
            }
        }

        public DateTime UploadDate
        {
            get { return _photo.UploadDate; }
            set
            {
                _photo.UploadDate = value;

                RaisePropertyChanged();
            }
        }


        #endregion

        #region Contruct

        public PhotoViewModel(Photo photo)
        {
            Debug.Assert(null != photo);

            _photo = photo;
        }

        #endregion

        #region Public Methods



        #endregion

        #region Private Methods



        #endregion
    }
}
