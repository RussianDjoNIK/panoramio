﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using GalaSoft.MvvmLight;
using Panoramio.Model;

namespace Panoramio.ViewModel.EntitiesVM
{
    public class ResponseViewModel : ViewModelBase
    {
        #region Fields

        private readonly Response _response;
        private readonly IEnumerable<PhotoViewModel> _photos;
        private readonly MapLocationViewModel _mapLocation;

        #endregion

        #region Properties

        public int Count
        {
            get { return _response.Count; }
            set
            {
                _response.Count = value;

                RaisePropertyChanged();
            }
        }

        public bool HasMore
        {
            get { return _response.HasMore; }
            set
            {
                _response.HasMore = value;

                RaisePropertyChanged();
            }
        }

        public MapLocationViewModel MapLocation
        {
            get { return _mapLocation; }
        }

        public IEnumerable<PhotoViewModel> Photos
        {
            get { return _photos; }
        }

        #endregion

        #region Contruct

        public ResponseViewModel(Response response)
        {
            Debug.Assert(null != response);
            Debug.Assert(null != response.MapLocation);

            _response = response;

            _mapLocation = new MapLocationViewModel(_response.MapLocation);

            _photos = _response.Photos.Any()
                ? _response.Photos.Select(p => new PhotoViewModel(p))
                : new List<PhotoViewModel>();
        }

        #endregion

        #region Public Methods



        #endregion

        #region Private Methods



        #endregion
    }
}
