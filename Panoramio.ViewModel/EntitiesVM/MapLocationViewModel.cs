﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using Panoramio.Model;

namespace Panoramio.ViewModel.EntitiesVM
{
    public class MapLocationViewModel : ViewModelBase
    {
        #region Fields

        private readonly MapLocation _mapLocation;

        #endregion

        #region Properties

        public double Longitude
        {
            get { return _mapLocation.Longitude; }
            set
            {
                _mapLocation.Longitude = value;

                RaisePropertyChanged();
            }

        }

        public double Latitude
        {
            get { return _mapLocation.Latitude; }
            set
            {
                _mapLocation.Latitude = value;

                RaisePropertyChanged();
            }

        }

        public int PanoramioZoom
        {
            get { return _mapLocation.PanoramioZoom; }
            set
            {
                _mapLocation.PanoramioZoom = value;

                RaisePropertyChanged();
            }

        }

        #endregion

        #region Contruct

        public MapLocationViewModel(MapLocation mapLocation)
        {
            Debug.Assert(null != mapLocation);

            _mapLocation = mapLocation;
        }

        #endregion

        #region Public Methods



        #endregion

        #region Private Methods



        #endregion
    }
}
