﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Panoramio.Common;

namespace Panoramio.Model
{
    public sealed class Response
    {
        [JsonProperty(Constants.DtoFields.Count)]
        public int Count { get; set; }

        [JsonProperty(Constants.DtoFields.HasMore)]
        public bool HasMore { get; set; }

        [JsonProperty(Constants.DtoFields.MapLocation)]
        public MapLocation MapLocation { get; set; }

        [JsonProperty(Constants.DtoFields.Photo)]
        public List<Photo> Photos { get; set; }

        [JsonIgnore]
        public string ErrorMessage { get; set; }
    }
}
