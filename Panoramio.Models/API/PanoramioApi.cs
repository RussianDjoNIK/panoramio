﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.Web.Http;
using Newtonsoft.Json;
using Panoramio.Common;

namespace Panoramio.Model.API
{
    public static class PanoramioApi
    {
        private const string PanoramioTemplate = "http://www.panoramio.com/map/get_panoramas.php?set={0}&from={1}&to={2}&minx={3}&miny={4}&maxx={5}&maxy={6}&size={7}&mapfilter={8}";
        
        //TODO need another approach
        static readonly Dictionary<SetKind, string> _setValues = new Dictionary<SetKind, string>
        {
            {SetKind.Public, Constants.ApiValues.Public},
            {SetKind.Full, Constants.ApiValues.Full},
            {SetKind.UserId, Constants.ApiValues.UserId}
        };

        //TODO need another approach
        static readonly Dictionary<SizeKind, string> _sizeValues = new Dictionary<SizeKind, string>
        {
            {SizeKind.Original, Constants.ApiValues.Original},
            {SizeKind.Medium, Constants.ApiValues.Medium},
            {SizeKind.Small, Constants.ApiValues.Small},
            {SizeKind.Thumbnail, Constants.ApiValues.Thumbnail},
            {SizeKind.Square, Constants.ApiValues.Square},
            {SizeKind.MiniSquare, Constants.ApiValues.MiniSquare}
        };

        public static async Task<Response> GetPanoramioResponce(double leftLon, double topLat, double rightLon, double bottomLat)
        {
            //TODO "public", images count ([0,20]), "original" and filter to settings
            var panoUrl = string.Format(PanoramioTemplate, "public", 0, 20, leftLon, bottomLat, rightLon, topLat, "original", true);
            Debug.WriteLine(panoUrl);

            Response response;
            try
            {
                HttpClient http;
                using (http = new HttpClient())
                {
                    var uri = new Uri(panoUrl);
                    var str = await http.GetStringAsync(uri);
                    response = JsonConvert.DeserializeObject<Response>(str);
                }
            }
            // TODO more specific exception
            catch (Exception ex)
            {
                //TODO probably more informative message
                response = new Response { ErrorMessage = ex.Message};
            }

            return response;
        }
    }
}
