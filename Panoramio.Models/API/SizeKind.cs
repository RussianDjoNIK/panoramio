﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panoramio.Model.API
{
    enum SizeKind
    {
        Original,
        Medium,
        Small,
        Thumbnail,
        Square,
        MiniSquare
    }
}
