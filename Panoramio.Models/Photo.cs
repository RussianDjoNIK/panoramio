﻿using System;
using Newtonsoft.Json;
using Panoramio.Common;

namespace Panoramio.Model
{
    public sealed class Photo
    {
        [JsonProperty(Constants.DtoFields.Height)]
        public double Height { get; set; }
        [JsonProperty(Constants.DtoFields.Width)]
        public double Width { get; set; }
        [JsonProperty(Constants.DtoFields.Latitude)]
        public double Latitude { get; set; }
        [JsonProperty(Constants.DtoFields.Longitude)]
        public double Longitude { get; set; }
        [JsonProperty(Constants.DtoFields.OwnerId)]
        public int OwnerId { get; set; }
        [JsonProperty(Constants.DtoFields.OwnerName)]
        public string OwnerName { get; set; }
        [JsonProperty(Constants.DtoFields.OwnerUrl)]
        public string OwnerUrl { get; set; }
        [JsonProperty(Constants.DtoFields.PhotoFileUrl)]
        public string PhotoFileUrl { get; set; }
        [JsonProperty(Constants.DtoFields.PhotoId)]
        public int PhotoId { get; set; }
        [JsonProperty(Constants.DtoFields.PhotoTitle)]
        public string PhotoTitle { get; set; }
        [JsonProperty(Constants.DtoFields.PhotoUrl)]
        public string PhotoUrl { get; set; }
        [JsonProperty(Constants.DtoFields.UploadDate)]
        public DateTime UploadDate { get; set; }
    }
}
