﻿using Newtonsoft.Json;
using Panoramio.Common;

namespace Panoramio.Model
{
    public sealed class MapLocation
    {
        [JsonProperty(Constants.DtoFields.Lat)]
        public double Latitude { get; set; }
        [JsonProperty(Constants.DtoFields.Lon)]
        public double Longitude { get; set; }
        [JsonProperty(Constants.DtoFields.PanoramioZoom)]
        public int PanoramioZoom { get; set; }
    }
}
